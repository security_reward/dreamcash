package com.munsang.market.util;

/**
 * Created by CK on 2017-06-26.
 */

public class Constants {
    public static final String HOST = "https://dreamad01.pe.kr:14002";
  /*  public static final String HOST = "http://tsd161109.pe.kr:14004";*/
/*
    public static final String HOST = "http://211.110.1.17:14004";
*/
    public static final String STATUS_BAR_COLOR = "#8DAAC6";
    public static final boolean DEBUG_MODE = false;

    // Shared Preference
    public static final String SP_NAME = "ShareName";
    public static final String SP_ID = "SharedId";
    public static final String SP_PWD = "SharedPWD";

    //
    public static final int SIGN_UP_TYPE_EMAIL = 0;
    public static final int SIGN_UP_TYPE_KAKAO = 1;

    // New
    public static final String SERVER_NEW_REGI_EMAIL_NICK_CHECK  = HOST + "/api/isDuplicate";
    public static final String SERVER_NEW_FIND_USERS  = HOST + "/api/findUsers";
    public static final String SERVER_NEW_REGI_SIGN_UP  = HOST + "/api/join";
    public static final String SERVER_NEW_LOGIN = HOST + "/api/login";
    public static final String SERVER_NEW_LOGOUT = HOST + "/api/logout";
    public static final String SERVER_NEW_SIGN_OUT = HOST + "/api/leave";
    public static final String SERVER_NEW_USE_POINT  = HOST + "/api/getUserPoint";
    public static final String SERVER_NEW_DOWNLOAD  = HOST + "/api/getUserPointHistory";
    public static final String SERVER_NEW_APP_ID_CHECK  = HOST + "/api/appId";
    public static final String SERVER_NEW_TICKET_LIST  = HOST + "/api/getPointGoods";
    public static final String SERVER_NEW_CONFIG  = HOST + "/api/config";
    public static final String SERVER_NEW_GET_POLICY  = HOST + "/api/getPolicy";
    public static final String SERVER_NEW_GET_AGREEMENT  = HOST + "/api/getAgreement";
    public static final String SERVER_NEW_BUY_HISTORY  = HOST + "/api/getBuyingHistory";
    public static final String SERVER_NEW_GET_INVITE  = HOST + "/api/getInvite";
    public static final String SERVER_NEW_GET_NOTICE  = HOST + "/api/notice";
    public static final String SERVER_NEW_GET_FAQ  = HOST + "/api/faq";
    public static final String SERVER_NEW_GET_QA  = HOST + "/api/qna";
    public static final String SERVER_NEW_REGI_QA  = HOST + "/api/setQna";


}
